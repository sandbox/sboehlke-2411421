== REQUIREMENTS ==
******************

* Date Module
  https://www.drupal.org/project/date

* jQuery Update with at least Version 1.7 activated
  https://drupal.org/project/jquery_update
  
* Libraries API
  https://drupal.org/project/libraries
  
* XDSoft Datetimepicker
  https://github.com/xdan/datetimepicker


== INSTALLATION ==
******************

1) Download the XDSoft Datetimepicker library from 
https://github.com/xdan/datetimepicker and  extract the archive in 
your libraries folder (sites/all/libraries). The name
of the folder containing all the files must be "datetimepicker", e.g.
"sites/all/libraries/datetimepicker".

2) Download and activate the module.

3) Now there is an option "Use DateTimePicker" (located under the point
"More settings and values") available when you edit your date fields.
This option is only visible for date field with the Widget Type "Text field".
