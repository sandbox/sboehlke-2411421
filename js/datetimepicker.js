/**
 * @file
 * Attaches the XDSoft DateTimePicker to date_text fields.
 */

(function($){
  Drupal.behaviors.dateTimePicker = {
    attach: function (context, settings) {
      if ($.fn.datetimepicker) {
        $.each(Drupal.settings.dateTimePicker, function(index, options) {
          var dateSelector = '#' + options.id + ' input';
          $(dateSelector, context).once('datetimepicker', function() {
            if (options.all_day) {
              all_day_element = $('input[name="' + options.all_day_id + '"]');

              // Get settings depending on "All Day".
              if (all_day_element.prop('checked')) {
                dateSettings = options.all_day_settings;
              } else {
                dateSettings = options.settings;
              }

              $(dateSelector).datetimepicker(dateSettings);
              var xdsoft_datetimepicker = $(dateSelector).data('xdsoft_datetimepicker');
              var xdsoft_datetime       = xdsoft_datetimepicker.data('xdsoft_datetime');

              // Format "All Day" in textfield.
              if ($(dateSelector).val() && all_day_element.prop('checked')) {
                  oldDateFormat = options.settings.format;
                  newDateFormat = options.all_day_settings.format;

                  dateInput = Date.parseDate($(dateSelector).val(), oldDateFormat);
                  if (xdsoft_datetime.isValidDate(dateInput)) {
                    $(dateSelector).val(dateInput.dateFormat(newDateFormat));
                  }
              }

              // Change format according to "All Day".
              all_day_element.change(function() {
                if ($(this).prop('checked')) {
                  oldDateFormat = options.settings.format;
                  newDateFormat = options.all_day_settings.format;

                  xdsoft_datetimepicker.setOptions(options.all_day_settings);
                } else {
                  oldDateFormat = options.all_day_settings.format;
                  newDateFormat = options.settings.format;

                  xdsoft_datetimepicker.setOptions(options.settings);
                }

                if ($(dateSelector).val()) {
                  dateInput = Date.parseDate($(dateSelector).val(), oldDateFormat);
                  if (xdsoft_datetime.isValidDate(dateInput)) {
                    $(dateSelector).val(dateInput.dateFormat(newDateFormat));
                  }
                }
              });
            } else {
              $(dateSelector).datetimepicker(options.settings);
            }
          });
        });
      }
    }
  };
})(jQuery);
